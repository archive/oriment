#!/usr/bin/env bash

set -e

echo ">> Building Libraries..."
mkdir -p www/js/lib
uglifyjs node_modules/markdown/lib/markdown.js --compress --screw-ie8 --define --stats --keep-fnames -o www/js/lib/markdown.js
uglifyjs node_modules/bootstrap/dist/js/bootstrap.js --compress --screw-ie8 --define --stats --keep-fnames -o www/js/lib/bootstrap.js
uglifyjs www/js/lib/* --compress --screw-ie8 --define --stats --keep-fnames -o www/js/libs.js
rm -rf www/js/lib

echo ">> Building JQuery..."
uglifyjs node_modules/jquery/dist/jquery.js --compress --screw-ie8 --define --stats --keep-fnames -o www/js/jquery.js

echo ">> Building Application JS..."
browserify -t [ babelify --presets [ es2015 react ] ] src/js/app.js -o www/js/app.js
# uglifyjs www/js/app.js --compress --screw-ie8 --define --stats --keep-fnames -o www/js/app.js
