import React from 'react';

export default class HomeScreen extends React.Component {
  render() {
    return (
      <span id="main">
        <div className="container jumbotron header">
          <h1 className="animated rubberBand">Oriment</h1>
          <h5>Orient documents and images automagically</h5>
        </div>
        <div className="container animated zoomInUp">
          <img src="img/compass.png" id="compass"/>
        </div>
        <footer>
          <a className="btn btn-success btn-lg animated pulse infinite">Open Document / Images</a>
          <input type="file" />
        </footer>
      </span>
    );
  }
};
