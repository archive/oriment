import './utils';
import React from 'react';
import HomeScreen from './components/homescreen';

document.addEventListener("deviceready", function () {
  function onSuccess(heading) {
    $('#compass').rotate(-heading.magneticHeading.toFixed(2));
  };

  function onError(compassError) {
    alert('Compass error: ' + compassError.code);
  };

  const options = {
    frequency: 100
  };

  var watchID = navigator.compass.watchHeading(onSuccess, onError, options);

}, false);

$('footer .btn').click(function(e) {
  $('footer input').trigger('click');
});

$('footer input').on('change', function () {
  const file = $(this)[0].files[0];
  let reader = new FileReader();
  reader.onload = function(e) {
    $('#compass').attr('src', e.target.result);
    $('#compass').css('width', '78vw');
    $('.header').hide();
  };
  reader.readAsDataURL(file);
});

React.render(<HomeScreen />, $('#app')[0]);
